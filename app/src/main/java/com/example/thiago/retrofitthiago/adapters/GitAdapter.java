package com.example.thiago.retrofitthiago.adapters;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thiago.retrofitthiago.R;
import com.example.thiago.retrofitthiago.models.Owner;
import com.example.thiago.retrofitthiago.models.Repositories;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by thiago on 26/06/17.
 */

public class GitAdapter extends RecyclerView.Adapter<GitAdapter.ViewHolder> {

    private List<Repositories> repositories;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView repositoryName;
        public TextView descriptionText;
        public TextView forksNumber;
        public TextView starsNumber;
        public ImageView ownerPhoto;
        public TextView ownerName;


        public ViewHolder(View view) {
            super(view);
            this.repositoryName = (TextView) view.findViewById(R.id.repo_name);
            this.descriptionText = (TextView) view.findViewById(R.id.repo_description);
            this.forksNumber = (TextView) view.findViewById(R.id.repo_forks);
            this.starsNumber = (TextView) view.findViewById(R.id.repo_stars);
            this.ownerPhoto = (ImageView) view.findViewById(R.id.owner_photo);
            this.ownerName = (TextView) view.findViewById(R.id.owner_name);
        }
    }

    public GitAdapter(List<Repositories> repositorios) {
        this.repositories = repositorios;
    }

    @Override
    public GitAdapter.ViewHolder onCreateViewHolder (ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_view_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.repositoryName = (TextView) view.findViewById(R.id.repo_name);
        viewHolder.descriptionText = (TextView) view.findViewById(R.id.repo_description);
        viewHolder.forksNumber = (TextView) view.findViewById(R.id.repo_forks);
        viewHolder.starsNumber = (TextView) view.findViewById(R.id.repo_stars);
        viewHolder.ownerPhoto = (ImageView) view.findViewById(R.id.owner_photo);
        viewHolder.ownerName = (TextView) view.findViewById(R.id.owner_name);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.repositoryName.setText(this.repositories.get(position).getName());
        viewHolder.descriptionText.setText(this.repositories.get(position).getDescription());
        viewHolder.forksNumber.setText(Integer.toString(this.repositories.get(position).getForks()));
        viewHolder.starsNumber.setText(Integer.toString(this.repositories.get(position).getStargazersCount()));
        Picasso.with(viewHolder.ownerPhoto.getContext()).load(this.repositories.get(position).getOwner().getAvatarUrl()).into(viewHolder.ownerPhoto);
        viewHolder.ownerName.setText(this.repositories.get(position).getOwner().getLogin());
    }

    @Override
    public int getItemCount() { return this.repositories.size(); }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        }
}

