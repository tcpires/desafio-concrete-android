package com.example.thiago.retrofitthiago.services;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.thiago.retrofitthiago.itemDecoration.SimpleDividerItemDecoration;
import com.example.thiago.retrofitthiago.R;
import com.example.thiago.retrofitthiago.adapters.EndlessRecyclerViewScrollListener;
import com.example.thiago.retrofitthiago.adapters.GitAdapter;
import com.example.thiago.retrofitthiago.models.Repositories;
import com.example.thiago.retrofitthiago.models.TopRepositories;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

        public static final String API_BASE_URL = "https://api.github.com/";

        final Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        final OkHttpClient.Builder okHttpClientBuilder = new  OkHttpClient.Builder();

        final Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson));

        final Retrofit retrofit = retrofitBuilder.client(okHttpClientBuilder.build()).build();

        final GitHubEndpointInterface gitHubEndpointInterface = retrofit.create(GitHubEndpointInterface.class);

        private RecyclerView recyclerView;
        private RecyclerView.LayoutManager layoutManager;
        private RecyclerView.Adapter adapter;

        private List<Repositories> repositories = new ArrayList<>();

        private EndlessRecyclerViewScrollListener scrollListener;

        private Integer page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new GitAdapter(repositories);
        layoutManager = new LinearLayoutManager(this);


        recyclerView = (RecyclerView) findViewById(R.id.repos_recicle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        Call <TopRepositories> topRepositoriesCall = gitHubEndpointInterface
                .getTopRepositories("language:Java", "stars", page);

        topRepositoriesCall.enqueue(new Callback<TopRepositories>() {
            @Override
            public void onResponse(Call<TopRepositories> call, final Response<TopRepositories> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        repositories.addAll(response.body().getRepositories());
                        adapter.notifyDataSetChanged();
                        recyclerView.setAdapter(adapter);
                    }
                });
            }
            @Override
            public void onFailure(Call<TopRepositories> call, Throwable t) {
                Log.i("Erro Geral", "Erro: " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Falha na Chamada dos repositórios: "
                                + t.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        });

        scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                loadNextDataFromApi(page);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
    }

    public void loadNextDataFromApi (int offset){

        if (page>=1){ page = page + 1;

            Call <TopRepositories> topRepositoriesCall = gitHubEndpointInterface
                    .getTopRepositories("language:Java", "stars", page);

            topRepositoriesCall.enqueue(new Callback<TopRepositories>() {
                @Override
                public void onResponse(Call<TopRepositories> call, final Response<TopRepositories> response) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            repositories.addAll(response.body().getRepositories());
                            adapter.notifyDataSetChanged();
                            }
                    });
                }
                @Override
                public void onFailure(Call<TopRepositories> call, Throwable t) {
                    Log.i("Erro Geral", "Erro: " + t.getMessage());
                    Toast.makeText(getApplicationContext(), "Falha na Chamada dos repositórios: "
                                    + t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        }else { }
  }
}
