package com.example.thiago.retrofitthiago.services;

import com.example.thiago.retrofitthiago.models.TopRepositories;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by thiago on 26/06/17.
 */

public interface GitHubEndpointInterface {

    //@GET("/search/repositories?q=language:Java&sort=stars&page=1")
    //Call<TopRepositories> getTopJavaRepos(@Query("page") Integer page);

    @GET("/search/repositories")
    Call<TopRepositories> getTopRepositories(@Query("q") String q, @Query("sort") String sort, @Query("page") Integer page);
}
